Datasheety
===

V této složce jsou uloženy dokumenty výrobců jednotlivých komponent. Nejsem autor těchto dokumentů. Neodpovídám za správnost ani aktuálnost.

This folder contains manufacturer's datasheets. I am not an author. I take no responsibility for factual accuracy. For up to date information i sugest checking manufacturers website:

- [microchip] (https://www.microchip.com/)
- [mikroe] (https://www.mikroe.com/)
- [winsen sensor] (https://www.winsen-sensor.com)