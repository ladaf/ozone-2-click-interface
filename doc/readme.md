Rozhraní pro připojení Ozone 2 Click do PC
===

Informace o používání rozhraní a samotného senzoru najdete v souboru [ReadMe.pdf](./ReadMe.pdf).

Ve složce [datasheety](./datasheety) je dokumentace výrobce k jednotlivým součástkám.