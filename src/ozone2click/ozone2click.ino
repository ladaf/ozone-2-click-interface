#include "ozone2click.h"

#define CS_PIN 10
#define MISO_PIN 12

const long DoMeGr = 800000;     // nastaví dolni mez grafu v okne "polt"
const long HoMeGr = 2500000;    // nastaví horni mez grafu v okne "polt"

const int Perioda = 1000;       // nastavi periodu zapisu dat ze senzoru v milisekundach

unsigned long Delic; 
Ozone2click sensor(MISO_PIN, CS_PIN);

void setup() {
  Serial.begin(9600);
  sensor.begin();

  Serial.println("min \t max \t U_delic");

  //ladici info
  //sensor.setDebug(true);
}

void loop() {
  Delic = sensor.readValue();
  bool ol = sensor.getOL();
  // Serial.print(0);
  Serial.print(DoMeGr);
  Serial.print('\t');
  // Serial.print(0b1111111111111111111111);
  Serial.print(HoMeGr);
  Serial.print('\t');
  
  Serial.println(Delic);
    
  delay(Perioda);
}
