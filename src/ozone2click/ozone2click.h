#ifndef _ozone2click_h_
#define _ozone2click_h_

#include "arduino.h"

class Ozone2click{
  
  private:
    byte MISO_pin, CS_pin;

    bool debug = false;
  
    bool oh;
    bool ol;
    unsigned long value;
    void readFromSensor();

  public:
    Ozone2click(byte MISO, byte CS);
    void begin();
    
    bool getDebug() const {return this->debug;};
    void setDebug(const bool newDebug) {this->debug = newDebug;};

    
    bool getOH() const {return this->oh;};
    bool getOL() const {return this->ol;};

    unsigned long getValue() const;
    unsigned long readValue();

    float readResistannce();
    float readConcentration();
};


#endif
