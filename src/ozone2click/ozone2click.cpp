#include "ozone2click.h"
#include <SPI.h>

Ozone2click::Ozone2click(byte MISO, byte CS):
  MISO_pin(MISO), CS_pin(CS)
{
  pinMode(this->CS_pin, OUTPUT);
}

void Ozone2click::begin(){
  SPI.begin();
}


void Ozone2click::readFromSensor(){
  SPI.beginTransaction(SPISettings(14000000, MSBFIRST, SPI_MODE3));

  byte incoming_byte = 0;
  this->value = 0;
  
  digitalWrite(this->CS_pin, LOW);
  delayMicroseconds(200);
  while(digitalRead(this->MISO_pin)){
  }
  for (int i = 0; i < 3; ++i){
    this->value = this->value << 8;
    incoming_byte = SPI.transfer(0x00);
    if (0 == i){
      this->oh = incoming_byte & 0b10000000;
      this->ol = incoming_byte & 0b01000000;
      incoming_byte &= 0b00111111;

      if(this->debug){
        Serial.print("OH: ");
        Serial.println(this->oh);
        Serial.print("OL: ");
        Serial.println(this->ol);
      }
    }
    this->value |= incoming_byte;
    if(this->debug){
      Serial.print("Byte: ");
      Serial.print(i);
      Serial.print(" : ");
      Serial.println(incoming_byte);
    }
  }
  digitalWrite(this->CS_pin, HIGH);
  SPI.endTransaction();
}


unsigned long Ozone2click::getValue()const{
  return this->value;
}
unsigned long Ozone2click::readValue(){
  this->readFromSensor();
  return this->getValue();
}
