Rozhraní pro připojení Ozone 2 Click do PC
===

Pro účely testování senzoru bylo vyvyinutio rozhraní pro přímé připojení modulu Ozone 2 Click se senzorem MQ131 a DA převodníkem.

Důraz byl kladen na jednoduchost a nenáročnost zapojení, rozraní lze realizovat s deskou Ardunino nano a univerzálním plošným spojem.